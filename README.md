### Sorting

The following performs an efficiency test on the most popular sorting algorithms using identical sets of pseudo-randomly ordered integer values.


### Integration

#### C++ Dependencies
The build script will try to locate all these dependencies for you:

* [CMake 3.14+](https://cmake.org)
* [Boost 1.67.0+](https://www.boost.org)
* [Gnuplot 5.2.6+](http://www.gnuplot.info) (required at runtime)

#### Python Dependencies

* [Numba 0.49.1](https://pypi.org/project/numba/0.49.1/)
* [Argparse 1.1](https://pypi.org/project/argparse/1.1/)
* [Matplotlib 3.1.3](https://pypi.org/project/matplotlib/3.1.3/)


### Usage

```bash
$ cmake .

$ make simulation

$ ./simulation <min_size> <max_size> <step_incr>
```


### Output
<p align="center"><img src="simulation.png" width="499"></p>
