from numba import jit
from random import randint
from insertion import sort as insertion_sort

@jit
def standardized_partition(arr, lowerbound, upperbound):
    pivot_index = lowerbound - 1
    pivot_value = arr[upperbound]

    for i in range(lowerbound, upperbound):
        if arr[i] <= pivot_value:
            pivot_index += 1
            arr[i], arr[pivot_index] = arr[pivot_index], arr[i]

    pivot_index += 1
    arr[upperbound], arr[pivot_index] =  arr[pivot_index], arr[upperbound]
    return pivot_index

@jit
def standardized_sort_helper(arr, lowerbound, upperbound):
    if lowerbound < upperbound:
        pivot = standardized_partition(arr, lowerbound, upperbound)
        standardized_sort_helper(arr, lowerbound, pivot-1)
        standardized_sort_helper(arr, pivot+1, upperbound)

@jit
def standardized_sort(arr):
    standardized_sort_helper(arr, 0, len(arr) - 1)

@jit
def randomized_partition(arr, lowerbound, upperbound):
    rand_index = randint(lowerbound, upperbound)
    arr[upperbound], arr[rand_index] = arr[rand_index], arr[upperbound]
    return standardized_partition(arr, lowerbound, upperbound)

@jit
def randomized_sort_helper(arr, lowerbound, upperbound):
    if lowerbound < upperbound:
        pivot = randomized_partition(arr, lowerbound, upperbound)
        randomized_sort_helper(arr, lowerbound, pivot-1)
        randomized_sort_helper(arr, pivot+1, upperbound)

@jit
def randomized_sort(arr):
    randomized_sort_helper(arr, 0, len(arr) - 1)

@jit
def non_recursive_sort_helper(arr, lowerbound, upperbound):
    stack = [(lowerbound, upperbound)]
    while len(stack) > 1:
        lowerbound, upperbound = stack.pop()
        pivot = randomized_partition(arr, lowerbound, upperbound)
        stack.append((lowerbound, pivot-1))
        stack.append((pivot+1, upperbound))

@jit
def non_recursive_sort(arr):
    non_recursive_sort_helper(arr, 0, len(arr) - 1)

@jit
def adaptive_sort(arr, threshold=100):
    if len(arr) <= threshold: insertion_sort(arr)
    else: randomized_sort(arr)
