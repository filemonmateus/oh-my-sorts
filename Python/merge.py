from numba import jit

@jit
def merge(left, right):
    merged = []
    left_index, right_index = 0, 0
    left_length, right_length = len(left), len(right)

    while left_index < left_length and right_index < right_length:
        if left[left_index] < right[right_index]:
            merged.append(left[left_index])
            left_index += 1
        else:
            merged.append(right[right_index])
            right_index += 1

    merged += left[left_index:]
    merged += right[right_index:]
    return merged

@jit
def sort(arr):
    arr_length = len(arr)

    if arr_length <= 1:
        return arr

    middle = arr_length // 2
    left = sort(arr[:middle])
    right = sort(arr[middle:])
    return merge(left, right)
