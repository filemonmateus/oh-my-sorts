from numba import jit

@jit
def sort(arr):
    arr_length = len(arr)

    for i in range(arr_length - 1):
        smallest_index = i

        for j in range(i+1, arr_length):
            if arr[j] < arr[smallest_index]:
                smallest_index = j

        if smallest_index != i:
            arr[smallest_index], arr[i] = arr[i], arr[smallest_index]
