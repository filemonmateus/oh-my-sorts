'''
Performs an efficiency benchmark on the most popular sorting algorithms
using identical sets of pseudo-randomly ordered integer values with
auto-vectorization and parallelization with numba. Runs on LLVM.
'''

import os
import sys
import time
import argparse
from utils import rand_values, benchmark
import bubble, insertion, merge, quick, selection, heap
import matplotlib.pyplot as plt

# [DEBUG] numba unprecedented warnings
import warnings
warnings.filterwarnings('ignore')

# setup global plotting params and add latex support
plt.rcParams['toolbar'] = 'None'
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 9
plt.rcParams['figure.figsize'] = (7,7)

def main():
    parser = argparse.ArgumentParser(prog='simulate',
                                    description='Performs an efficiency benchmark on\
                                                the most popular sorting algorithms\
                                                using identical sets of pseudo-randomly\
                                                ordered integer values.',
                                    add_help=True)
    parser.add_argument('min', type=int, help='Minimum array size.')
    parser.add_argument('max', type=int, help='Maximum array size.')
    parser.add_argument('inc', type=int, help='Step size increments.')
    args = parser.parse_args()

    if not (args.min and args.max and args.inc):
        sys.exit(parser.usage)

    # parse args
    min = args.min
    max = args.max
    inc = args.inc
    size = (max - min) // inc

    # set test results
    heap_time = []
    merge_time = []
    std_quick_time = []
    rand_quick_time = []
    non_recursive_quick_time = []
    adaptive_quick_time = []
    insertion_time = []
    selection_time = []
    bubble_time = []
    sizes = []

    # commence simulation
    tic = time.time()
    for idx in range(min, max+1, inc):
        print(f'Test {(idx-min)//inc:02d}/{size} in progress. Please wait...')
        sizes.append(idx)

        # record test results
        values = rand_values(idx, min, max)
        heap_time.append(benchmark(heap.sort, values.copy()))
        merge_time.append(benchmark(merge.sort, values.copy()))
        std_quick_time.append(benchmark(quick.standardized_sort, values.copy()))
        rand_quick_time.append(benchmark(quick.randomized_sort, values.copy()))
        non_recursive_quick_time.append(benchmark(quick.non_recursive_sort, values.copy()))
        adaptive_quick_time.append(benchmark(quick.adaptive_sort, values.copy()))
        insertion_time.append(benchmark(insertion.sort, values.copy()))
        selection_time.append(benchmark(selection.sort, values.copy()))
        bubble_time.append(benchmark(bubble.sort, values))

        # animate plot
        plt.cla()
        plt.plot(sizes, heap_time, 'b--o', lw=1, label='Heap Sort')
        plt.plot(sizes, merge_time, 'k--o', lw=1, label='Merge Sort')
        plt.plot(sizes, std_quick_time, 'y--o', lw=1, label='Standardized Quick Sort')
        plt.plot(sizes, rand_quick_time, 'c--o', lw=1, label='Randomized Quick Sort')
        plt.plot(sizes, non_recursive_quick_time, 'm--s', lw=1, label='Non-Recursive Quick Sort')
        plt.plot(sizes, adaptive_quick_time, 'y--s', lw=1, label='Adaptive Quick Sort')
        plt.plot(sizes, insertion_time, 'g--o', lw=1, label='Insertion Sort')
        plt.plot(sizes, selection_time, 'r--o', lw=1, label='Selection Sort')
        plt.plot(sizes, bubble_time, 'm--o', lw=1, label='Bubble Sort')
        plt.xlim(plt.xlim()[0], max + plt.xlim()[0])
        plt.xlabel(r'\textbf{Array Size}', y=-1.01)
        plt.ylabel(r'\textbf{Sorting Time (in secs)}')
        plt.title(r'\textbf{Time Performance and Related Benchmarks of Sorting Algorithms}', y=1.01)
        plt.grid(linestyle='--', alpha=0.5)
        plt.legend(loc='best')
        plt.pause(0.01)

    toc = time.time()
    print('Simulation finished!')

    # save test results
    plt.savefig(f'{os.path.basename(__file__).split(".")[0]}.svg', format='svg')
    plt.show()

    # output benchmarks
    print(f'Took {toc-tic:.3f} seconds.')

if __name__ == '__main__':
    main()
