from numba import jit

@jit
def sort(arr):
    scans =  0
    arr_length = len(arr)
    sorted = False

    while not sorted:
        sorted = True

        for i in range(arr_length - scans - 1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                sorted = False

        scans += 1
