from numba import jit

@jit
def heapify(arr, size, idx):
    max, left, right = idx, 2*idx, 2*idx+1
    if left < size and arr[max] < arr[left]: max = left
    if right < size and arr[max] < arr[right]: max = right
    if idx != max:
        arr[idx], arr[max] = arr[max], arr[idx]
        heapify(arr, size, max)

@jit
def sort(arr):
    arr_length = len(arr)

    for idx in range(arr_length // 2, -1, -1):
        heapify(arr, arr_length, idx)

    for idx in range(arr_length - 1, -1, -1):
        arr[idx], arr[0] = arr[0], arr[idx]
        heapify(arr, idx, 0)
