import time
from numba import jit
from random import randint

@jit
def rand_values(size, lowerbound, upperbound):
    rand_vals = [randint(lowerbound, upperbound) for _ in range(size)]
    return rand_vals

@jit
def benchmark(algorithm, arr):
    tic = time.time()
    algorithm(arr)
    toc = time.time()
    return toc - tic
