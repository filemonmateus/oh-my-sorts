from numba import jit

@jit
def sort(arr):
    arr_length = len(arr)

    for i in range(1, arr_length):
        current_index = i
        current_value = arr[i]

        while current_index > 0 and arr[current_index-1] > current_value:
            arr[current_index] = arr[current_index-1]
            current_index -= 1

        arr[current_index] = current_value
