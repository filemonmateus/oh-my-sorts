#ifndef BUBBLE_HPP
#define BUBBLE_HPP

#include <algorithm>
#include <vector>

// --- interfaces / prototypes ---
namespace bubble { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
