#ifndef SELECTION_HPP
#define SELECTION_HPP

#include <algorithm>
#include <vector>

// --- interfaces / prototypes ---
namespace selection { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
