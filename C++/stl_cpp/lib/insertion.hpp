#ifndef INSERTION_HPP
#define INSERTION_HPP

#include <algorithm>
#include <vector>

// --- interfaces / prototypes ---
namespace insertion { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
