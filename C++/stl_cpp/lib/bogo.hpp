#ifndef BOGO_HPP
#define BOGO_HPP

#include <algorithm>
#include <random>

// --- interfaces / prototypes ---
namespace bogo { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
