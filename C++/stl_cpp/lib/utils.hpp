#ifndef UTILS_HPP
#define UTILS_HPP

#include <functional>
#include <random>
#include <vector>
#include <chrono>

// --- interfaces / prototypes ---
namespace utils {
  std::vector<int> random_numbers (const int unsigned size);
  double benchmark (
    const std::function<void (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound)>& algorithm,
    std::vector<int> values
  );
}

#endif
