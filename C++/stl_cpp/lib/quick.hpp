#ifndef QUICK_HPP
#define QUICK_HPP

#include <algorithm>
#include <random>
#include <vector>
#include <stack>
#include <chrono>
#include "insertion.hpp"

// --- interfaces / prototypes ---
namespace quick {
  void standardized_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound);
  void randomized_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound);
  void non_recursive_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound);
  void adaptive_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound);
}

#endif
