#ifndef MERGE_HPP
#define MERGE_HPP

#include <algorithm>
#include <vector>

// --- interfaces / prototypes ---
namespace merge { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
