#ifndef HEAP_HPP
#define HEAP_HPP

#include <algorithm>
#include <vector>

// --- interfaces / prototypes ---
namespace heap { void sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound); }

#endif
