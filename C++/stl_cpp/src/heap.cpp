#include "../lib/heap.hpp"

void heap::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  std::make_heap(lowerbound, upperbound);
  for (std::vector<int>::iterator iter = upperbound; iter >= lowerbound + 2; --iter)
    std::pop_heap(lowerbound, iter);
}
