#include "../lib/merge.hpp"

// bug fixed and expected monotonically increasing permutation of values is now generated in this subroutine!
void merge::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  if (upperbound - lowerbound > 1) {
    std::vector<int>::iterator middle = lowerbound + 0.5 * std::distance(lowerbound, upperbound);
    merge::sort(lowerbound, middle);
    merge::sort(middle, upperbound);
    std::inplace_merge(lowerbound, middle, upperbound);
  }
}
