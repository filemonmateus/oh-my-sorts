#include "../lib/bogo.hpp"

void bogo::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  while (!std::is_sorted(lowerbound, upperbound)) {
    std::shuffle(lowerbound, upperbound, std::random_device());
  }
}
