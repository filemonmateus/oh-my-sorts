#include "../lib/bubble.hpp"

void bubble::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  int scans = 0;
  bool swapped;

  do {
    swapped = false;
    for (std::vector<int>::iterator iter = lowerbound; iter != upperbound - scans - 1; ++iter) {
      if (*(iter+1) < *iter) {
        std::iter_swap(iter+1, iter);
        swapped = true;
      }
    }
    ++scans;
  }
  while (swapped);
}
