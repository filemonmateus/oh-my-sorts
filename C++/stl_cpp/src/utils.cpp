#include "../lib/utils.hpp"

const auto random_numbers_between = [] (const int lowerbound, const int upperbound) {
  return [distribution = std::uniform_int_distribution<int>(lowerbound, upperbound), generator = std::mt19937_64 { std::random_device {} () }] () mutable {
    return distribution(generator);
  };
};

std::vector<int> utils::random_numbers (const unsigned int size) {
  std::vector<int> values(size);
  std::generate(values.begin(), values.end(), random_numbers_between(1, size));
  return values;
}

double utils::benchmark (
  const std::function<void (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound)>& algorithm,
  std::vector<int> values
)
{
  std::chrono::time_point<std::chrono::high_resolution_clock> tic = std::chrono::high_resolution_clock::now();
  algorithm(values.begin(), values.end());
  std::chrono::time_point<std::chrono::high_resolution_clock> toc = std::chrono::high_resolution_clock::now();
  return std::chrono::duration<double>(toc-tic).count();
}
