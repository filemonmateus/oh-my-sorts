#include "../lib/quick.hpp"

const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
std::default_random_engine seeder(seed);
std::mt19937_64 generator(seeder());

const auto rand_iter = [] (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  std::uniform_int_distribution<int> distribution(0, std::distance(lowerbound, upperbound) - 1);
  return lowerbound + distribution(generator);
};

void quick::standardized_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  if (upperbound - lowerbound > 1) {
    std::vector<int>::iterator pivot = std::partition(lowerbound, upperbound, [lowerbound] (const int& element) -> bool { 
      return element <= *lowerbound;
    });
    std::iter_swap(lowerbound, pivot-1);
    quick::standardized_sort(lowerbound, pivot-1);
    quick::standardized_sort(pivot, upperbound);
  }
}

void quick::randomized_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  if (upperbound - lowerbound > 1) {
    std::iter_swap(lowerbound, rand_iter(lowerbound, upperbound));
    std::vector<int>::iterator pivot = std::partition(lowerbound, upperbound, [lowerbound] (const int& element) -> bool { 
      return element <= *lowerbound;
    });
    std::iter_swap(lowerbound, pivot-1);
    quick::randomized_sort(lowerbound, pivot-1);
    quick::randomized_sort(pivot, upperbound);
  }
}

void quick::non_recursive_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  std::stack<std::pair<std::vector<int>::iterator, std::vector<int>::iterator>> stack = {};
  stack.push({lowerbound, upperbound});
  while (!stack.empty()) {
    std::vector<int>::iterator lowerbound = stack.top().first, upperbound = stack.top().second; stack.pop();
    if (upperbound - lowerbound > 1) {
      std::iter_swap(lowerbound, rand_iter(lowerbound, upperbound));
      std::vector<int>::iterator pivot = std::partition(lowerbound, upperbound, [lowerbound] (const int& element) -> bool {
        return element <= *lowerbound;
      });
      std::iter_swap(lowerbound, pivot-1);
      stack.push({lowerbound, pivot-1});
      stack.push({pivot, upperbound});
    }
  }
}

void quick::adaptive_sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  if (std::distance(lowerbound, upperbound) <= 100) insertion::sort(lowerbound, upperbound);
  else quick::randomized_sort(lowerbound, upperbound);
}
