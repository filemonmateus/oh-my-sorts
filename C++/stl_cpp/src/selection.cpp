#include "../lib/selection.hpp"

void selection::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  for (std::vector<int>::iterator iter1 = lowerbound; iter1 != upperbound - 1; ++iter1) {
    std::vector<int>::iterator min_iter = iter1;
    for (std::vector<int>::iterator iter2 = iter1 + 1; iter2 != upperbound; ++iter2) {
      if (*iter2 < *min_iter) {
        min_iter = iter2;
      }
    }
    if (iter1 != min_iter) {
      std::iter_swap(iter1, min_iter);
    }
  }
}
