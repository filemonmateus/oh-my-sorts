#include "../lib/insertion.hpp"

void insertion::sort (const std::vector<int>::iterator& lowerbound, const std::vector<int>::iterator& upperbound) {
  for (std::vector<int>::iterator iter = lowerbound + 1; iter != upperbound; ++iter) {
    std::vector<int>::iterator curr_iter = iter;
    int curr_val = *iter;
    while (curr_iter > lowerbound && *(curr_iter-1) > curr_val) {
      *curr_iter = *(curr_iter-1);
      --curr_iter;
    }
    *curr_iter = curr_val;
  }
}
