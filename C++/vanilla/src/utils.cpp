#include "../lib/utils.hpp"

const auto random_numbers_between = [] (const int lowerbound = std::numeric_limits<int>::min(), const int upperbound = std::numeric_limits<int>::max()) {
  return [ distribution = std::uniform_int_distribution<int>(lowerbound, upperbound), generator = std::mt19937_64 { std::random_device {} () }] () mutable {
    return distribution(generator);
  };
};

std::vector<int> utils::random_values (const unsigned int size) {
  std::vector<int> values(size);
  std::generate(values.begin(), values.end(), random_numbers_between(1, size));
  return values;
}

double utils::benchmark (
  const std::function<void (std::vector<int> values)>& algorithm,
  std::vector<int> values
) 
{
  std::chrono::time_point<std::chrono::high_resolution_clock> tic = std::chrono::high_resolution_clock::now();
  algorithm(values);
  std::chrono::time_point<std::chrono::high_resolution_clock> toc = std::chrono::high_resolution_clock::now();
  return std::chrono::duration<double>(toc-tic).count();
}
