#include "../lib/pancake.hpp"

void pancake::swap (int& x, int& y) {
  int tmp = x;
  x = y;
  y = tmp;
}

void pancake::flip (std::vector<int>& values, int k) {
  for (int i = 0; i < k/2; ++i) { 
    pancake::swap(values[i], values[k-i-1]);
  }
}

int pancake::idx_of_max (std::vector<int>& values, int size) {
  int max_idx = size;

  for (int i = size-1; i >= 0; --i)
    if (values[max_idx] < values[i])
      max_idx = i;

  return max_idx;
}

void pancake::sort (std::vector<int> values) {
  for (int i = values.size() - 1; i > 0; --i) {
    pancake::flip(values, pancake::idx_of_max(values, i) + 1);
    pancake::flip(values, i + 1);
  }
}
