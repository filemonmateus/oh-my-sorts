#include "../lib/merge.hpp"

// auxiliary space
std::vector<int> merged(max_size);

// generic merge subroutine with auxiliary linear space on the following bounds: [left_lowerbound, left_upperbound] U [right_lowerbound, right_upperbound]
void merge::merge (std::vector<int>& values, int left_lowerbound, int left_upperbound, int right_lowerbound, int right_upperbound) {
  int curr_idx = left_lowerbound, vec_size = right_upperbound - left_lowerbound;

  while (left_lowerbound <= left_upperbound && right_lowerbound <= right_upperbound)
    merged[curr_idx++] = values[left_lowerbound] < values[right_lowerbound] ? values[left_lowerbound++] : values[right_lowerbound++];

  while (left_lowerbound <= left_upperbound)
    merged[curr_idx++] = values[left_lowerbound++];

  while (right_lowerbound <= right_upperbound)
    merged[curr_idx++] = values[right_lowerbound++];

  for (int i = 0, j = right_upperbound; i <= vec_size; ++i, --j)
    values[j] = merged[j];
}

void merge::sort_helper (std::vector<int>& values, int lowerbound, int upperbound) {
  if (lowerbound < upperbound) {
    int middle = lowerbound + 0.5 * (upperbound - lowerbound);
    merge::sort_helper(values, lowerbound, middle);
    merge::sort_helper(values, middle + 1, upperbound);
    merge::merge(values, lowerbound, middle, middle + 1, upperbound);
  }
}

void merge::sort (std::vector<int> values) {
  merge::sort_helper(values, 0, values.size() - 1);
}
