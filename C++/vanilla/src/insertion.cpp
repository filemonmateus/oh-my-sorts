#include "../lib/insertion.hpp"

void insertion::sort (std::vector<int> values) {
  for (int i = 1, n = values.size(); i < n; ++i) {
    int current_index = i;
    int current_value = values[i];

    // slides elements to right to make room for current_value
    while (current_index > 0 && values[current_index-1] > current_value) {
      values[current_index] = values[current_index-1];
      current_index--;
    }

    values[current_index] = current_value;
  }
}
