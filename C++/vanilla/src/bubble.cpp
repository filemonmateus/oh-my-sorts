#include "../lib/bubble.hpp"

void bubble::swap (int& x, int& y) {
  int tmp = x;
  x = y;
  y = tmp;
}

void bubble::sort (std::vector<int> values) {
  int scans = 0;
  bool swapped;

  do {
    swapped = false;
    for (int i = 0, n = values.size(); i < n - scans - 1; ++i) {
      if (values[i+1] < values[i]) {
        bubble::swap(values[i+1], values[i]);
        swapped = true;
      }
    }
    ++scans;
  } 
  while(swapped);
}
