#include "../lib/selection.hpp"

void selection::swap (int& x, int& y) {
  int tmp = x;
  x = y;
  y = tmp;
}

void selection::sort (std::vector<int> values) {
  for (int i = 0, n = values.size(); i < n - 1; ++i) {
    int smallest_index = i;

    for (int j = i+1; j < n; ++j) {
      if (values[j] < values[smallest_index]) {
        smallest_index = j;
      }
    }

    if (smallest_index != i) {
      selection::swap(values[smallest_index], values[i]);
    }
  }
}
