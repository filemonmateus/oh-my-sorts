/**
 * Performs an efficiency test on the most popular sorting algorithms
 * using identical sets of pseudo-randomly ordered integer values.
 **/

#include <chrono>
#include <thread>
#include <future>
#include <vector>
#include <iostream>

#include <boost/assert.hpp>
#include <boost/assign.hpp>
#include <boost/lexical_cast.hpp>

#include "../lib/bogo.hpp"
#include "../lib/heap.hpp"
#include "../lib/merge.hpp"
#include "../lib/quick.hpp"
#include "../lib/bubble.hpp"
#include "../lib/pancake.hpp"
#include "../lib/insertion.hpp"
#include "../lib/selection.hpp"
#include "../lib/utils.hpp"

#include <matplot/matplot.h>

int main (const int argc, const char** argv) {
  // ensure proper usage
  if (argc != 4) {
    std::cerr << "USAGE: " << argv[0] << " <min> <max> <inc>" << std::endl;
    return EXIT_FAILURE;
  }

  // parse (and cast) args
  int min, max, inc, size;
  try {
    min = boost::lexical_cast<int>(argv[1]);
    max = boost::lexical_cast<int>(argv[2]);
    inc = boost::lexical_cast<int>(argv[3]);
    size = (max-min)/inc;
  }
  catch (const boost::bad_lexical_cast& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  // ensure size is at most 2^20 entries
  assert(size <= max);

  // set test results
  std::vector<double> heap_time;
  std::vector<double> merge_time;
  std::vector<double> std_quick_time;
  std::vector<double> rand_quick_time;
  std::vector<double> non_recursive_quick_time;
  std::vector<double> adaptive_quick_time;
  std::vector<double> insertion_time;
  std::vector<double> selection_time;
  std::vector<double> bubble_time;
  std::vector<double> sizes;
  std::vector<int> values;

  // and related threads
  std::future<double> heap_thread;
  std::future<double> merge_thread;
  std::future<double> std_quick_thread;
  std::future<double> rand_quick_thread;
  std::future<double> non_recursive_quick_thread;
  std::future<double> adaptive_quick_thread;
  std::future<double> insertion_thread;
  std::future<double> selection_thread;
  std::future<double> bubble_thread;

  // setup plot for visualization animation
  auto plt = matplot::gca();

  // commence simulation
  std::chrono::time_point<std::chrono::high_resolution_clock> tic = std::chrono::high_resolution_clock::now();
  for (int index = min, test = (index - min)/inc; index <= max; index += inc, ++test) {
    std::cout << "Test " << test << "/" << size << " in progress. Please wait..." << std::endl;
    sizes.push_back(index);
    values = utils::random_values(index);

    // multi-threaded test
    heap_thread = std::async(&utils::benchmark, heap::sort, values);
    merge_thread = std::async(&utils::benchmark, merge::sort, values);
    std_quick_thread = std::async(&utils::benchmark, quick::standardized_sort, values);  
    rand_quick_thread = std::async(&utils::benchmark, quick::randomized_sort, values);  
    non_recursive_quick_thread = std::async(&utils::benchmark, quick::non_recursive_sort, values);  
    adaptive_quick_thread = std::async(&utils::benchmark, quick::adaptive_sort, values);  
    insertion_thread = std::async(&utils::benchmark, insertion::sort, values);  
    selection_thread = std::async(&utils::benchmark, selection::sort, values);  
    bubble_thread = std::async(&utils::benchmark, bubble::sort, values);

    // record test results
    heap_time.push_back(heap_thread.get());
    merge_time.push_back(merge_thread.get());
    std_quick_time.push_back(std_quick_thread.get());
    rand_quick_time.push_back(rand_quick_thread.get());
    non_recursive_quick_time.push_back(non_recursive_quick_thread.get());
    adaptive_quick_time.push_back(adaptive_quick_thread.get());
    insertion_time.push_back(insertion_thread.get());
    selection_time.push_back(selection_thread.get());
    bubble_time.push_back(bubble_thread.get());

    // animate plot
    plt->clear();
    plt->hold(true);
    plt->plot(sizes, heap_time, "--bo")->line_width(1.3).marker_size(5).marker_color("b").marker_face_color("b").display_name("Heap Sort");
    plt->plot(sizes, merge_time, "--ko")->line_width(1.3).marker_size(5).marker_color("k").marker_face_color("k").display_name("Merge Sort");
    plt->plot(sizes, std_quick_time, "--yo")->line_width(1.3).marker_size(5).marker_color("y").marker_face_color("y").display_name("Standardized Quick Sort");
    plt->plot(sizes, rand_quick_time, "--co")->line_width(1.3).marker_size(5).marker_color("c").marker_face_color("c").display_name("Randomized Quick Sort");
    plt->plot(sizes, non_recursive_quick_time, "--ms")->line_width(1.3).marker_size(5).marker_color("m").marker_face_color("m").display_name("Non-Recursive Quick Sort");
    plt->plot(sizes, adaptive_quick_time, "--ys")->line_width(1.3).marker_size(5).marker_color("y").marker_face_color("y").display_name("Adaptive Quick Sort");
    plt->plot(sizes, insertion_time, "--go")->line_width(1.3).marker_size(5).marker_color("g").marker_face_color("g").display_name("Insertion Sort");
    plt->plot(sizes, selection_time, "--ro")->line_width(1.3).marker_size(5).marker_color("r").marker_face_color("r").display_name("Selection Sort");
    plt->plot(sizes, bubble_time, "--mo")->line_width(1.3).marker_size(5).marker_color("m").marker_face_color("m").display_name("Bubble Sort");
    plt->xlim({ boost::lexical_cast<double>(min-inc), boost::lexical_cast<double>(max+inc) });
    plt->xlabel("Array Size");
    plt->ylabel("Sorting Time (in secs)");
    plt->title("Time Performance and Related Benchmarks of Sorting Algorithms");
    plt->grid(true);
    matplot::legend(true)->location(matplot::legend::general_alignment::topleft);
  }
  std::chrono::time_point<std::chrono::high_resolution_clock> toc = std::chrono::high_resolution_clock::now();
  std::cout << "Simulation finished!" << std::endl;

  // save test results
  matplot::save(boost::lexical_cast<std::string>(argv[0]), "svg");

  // output benchmarks
  std::cout << "Took: " << std::chrono::duration<double>(toc-tic).count() << " seconds." << std::endl;

  // that's all folks!
  return EXIT_SUCCESS;
}
