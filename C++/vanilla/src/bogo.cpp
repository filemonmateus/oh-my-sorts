#include "../lib/bogo.hpp"

int bogo::rand_idx (const int lowerbound, const int upperbound) {
  const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine seeder(seed);
  std::mt19937_64 generator(seeder());
  std::uniform_int_distribution<int> distribution(lowerbound, upperbound);
  return distribution(generator);
}

void bogo::shuffle (std::vector<int>& values) {
  for (int i = 0, n = values.size(); i < n; ++i) {
    std::swap(values[i], values[bogo::rand_idx(0, n-1)]);
  }
}

bool bogo::sorted (const std::vector<int>& values) {
  for (int i = 0, n = values.size(); i < n-1; ++i) {
    if (values[i+1] < values[i]) {
      return false;
    }
  }
  return true;
}

void bogo::sort (std::vector<int> values) {
  while(!bogo::sorted(values)) {
    bogo::shuffle(values);
  }
}
