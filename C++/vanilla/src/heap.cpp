#include "../lib/heap.hpp"

void heap::swap (int& x, int& y) {
  int tmp = x;
  x = y;
  y = tmp;
}

void heap::maxheap (std::vector<int>& values) {
  for (int i = values.size()/2; i >= 0; --i) {
    heap::heapify(values, values.size(), i);
  }
} 

void heap::heapify (std::vector<int>& values, int size, int idx) {
  int max = idx, left = 2*idx, right = 2*idx+1;
  if (left < size && values[max] < values[left]) max = left;
  if (right < size && values[max] < values[right]) max = right;
  if (max != idx) { heap::swap(values[idx], values[max]); heap::heapify(values, size, max); }
}

void heap::sort (std::vector<int> values) {
  heap::maxheap(values);
  for (int i = values.size()-1; i >= 0; --i) {
    heap::swap(values[i], values[0]); heap::heapify(values, i, 0);
  }
}
