#include "../lib/quick.hpp"

const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
std::default_random_engine seeder(seed);
std::mt19937_64 generator(seeder());

void quick::swap (int& x, int& y) {
  int tmp = x;
  x = y;
  y = tmp;
}

int quick::random_index (const int lowerbound, const int upperbound) {
  std::uniform_int_distribution<int> distribution(lowerbound, upperbound);
  return distribution(generator);
}

int quick::standardized_partition (std::vector<int>& values, const int lowerbound, const int upperbound) {
  int pivot_index = lowerbound - 1;
  int pivot_value = values[upperbound];
  for (int i = lowerbound; i < upperbound; ++i) {
    if (values[i] <= pivot_value) {
      quick::swap(values[i], values[++pivot_index]);
    } 
  }
  quick::swap(values[upperbound], values[++pivot_index]);
  return pivot_index;
}

void quick::standardized_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound) {
  if (lowerbound < upperbound) {
    int pivot = quick::standardized_partition(values, lowerbound, upperbound);
    quick::standardized_sort_helper(values, lowerbound, pivot-1);
    quick::standardized_sort_helper(values, pivot+1, upperbound);
  }
}

void quick::standardized_sort (std::vector<int> values) {
  quick::standardized_sort_helper(values, 0, values.size() - 1);
}

int quick::randomized_partition (std::vector<int>& values, const int lowerbound, const int upperbound) {
  int rand_index = quick::random_index(lowerbound, upperbound);
  quick::swap(values[upperbound], values[rand_index]);
  return quick::standardized_partition(values, lowerbound, upperbound);
}

void quick::randomized_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound) {
  if (lowerbound < upperbound) {
    int pivot = quick::randomized_partition(values, lowerbound, upperbound);
    quick::randomized_sort_helper(values, lowerbound, pivot-1);
    quick::randomized_sort_helper(values, pivot+1, upperbound);
  }
}

void quick::randomized_sort (std::vector<int> values) {
  quick::randomized_sort_helper(values, 0, values.size() - 1);
}

void quick::non_recursive_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound) {
  std::stack<std::pair<int, int>> stack = {};
  stack.push({lowerbound, upperbound});
  while (!stack.empty()) {
    auto [lowerbound, upperbound] = stack.top(); stack.pop();
    if (lowerbound < upperbound) {
      int pivot = randomized_partition(values, lowerbound, upperbound);
      stack.push({lowerbound, pivot-1});
      stack.push({pivot+1, upperbound});
    }
  }
}

void quick::non_recursive_sort (std::vector<int> values) {
  quick::non_recursive_sort_helper(values, 0, values.size() - 1);
}

void quick::adaptive_sort (std::vector<int> values) {
  if (values.size() <= 100) insertion::sort(values);
  else quick::randomized_sort(values);
}
