#ifndef PANCAKE_HPP
#define PANCAKE_HPP

#include <vector>

// --- interfaces / prototypes ---
namespace pancake {
  void swap (int& x, int& y);
  void flip (std::vector<int>& values, int k);
  int idx_of_max (std::vector<int>& values, int size);
  void sort (std::vector<int> values);
}

#endif
