#ifndef SELECTION_HPP
#define SELECTION_HPP

#include <vector>

// --- interfaces / prototypes ---
namespace selection {
  void swap (int& x, int& y);
  void sort (std::vector<int> values);
}

#endif
