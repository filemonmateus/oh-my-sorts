#ifndef BUBBLE_HPP
#define BUBBLE_HPP

#include <vector>

// --- interfaces / prototypes ---
namespace bubble {
  void swap (int& x, int& y);
  void sort (std::vector<int> values);
}

#endif
