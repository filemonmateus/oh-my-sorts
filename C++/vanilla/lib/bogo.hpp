#ifndef BOGO_HPP
#define BOGO_HPP

#include <vector>
#include <random>
#include <iostream>
#include <chrono>

// --- interfaces / prototypes ---
namespace bogo {
  int rand_idx (const int lowerbound, const int upperbound);
  void shuffle (std::vector<int>& values); // --- fisher-yates linear shuffle ---
  bool sorted (const std::vector<int>& values);
  void sort (std::vector<int> values);
}

#endif
