#ifndef QUICK_HPP
#define QUICK_HPP

#include <stack>
#include <vector>
#include <random>
#include <chrono>
#include "insertion.hpp"

// --- interfaces / prototypes ---
namespace quick {
  void swap (int& x, int& y);
  int random_index (const int lowerbound, const int upperbound);

  int standardized_partition (std::vector<int>& values, const int lowerbound, const int upperbound);
  void standardized_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound);
  void standardized_sort (std::vector<int> values);

  int randomized_partition (std::vector<int>& values, const int lowerbound, const int upperbound);
  void randomized_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound);
  void randomized_sort (std::vector<int> values);

  void non_recursive_sort_helper (std::vector<int>& values, const int lowerbound, const int upperbound);
  void non_recursive_sort (std::vector<int> values);
  void adaptive_sort (std::vector<int> values);
}

#endif
