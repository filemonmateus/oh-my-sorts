#ifndef UTILS_HPP
#define UTILS_HPP

#include <chrono>
#include <vector>
#include <random>
#include <limits>
#include <functional>
#include <algorithm>

// --- interfaces / prototypes ---
namespace utils {
  std::vector<int> random_values (const unsigned int size);
  double benchmark (
    const std::function<void (std::vector<int> values)>& algorithm,
    std::vector<int> values
  );
}

#endif
