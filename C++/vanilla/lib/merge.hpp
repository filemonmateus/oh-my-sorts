#ifndef MERGE_HPP
#define MERGE_HPP

#include <vector>

const int max_size = 1 << 20;

// --- interfaces / prototypes ---
namespace merge {
  void merge (std::vector<int>& values, int lowerbound1, int upperbound1, int lowerbound2, int upperbound2);
  void sort_helper (std::vector<int>& values, int lowerbound, int upperbound);
  void sort (std::vector<int> values);
}

#endif
