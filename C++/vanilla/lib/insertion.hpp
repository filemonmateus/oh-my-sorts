#ifndef INSERTION_HPP
#define INSERTION_HPP

#include <vector>

// --- interfaces / prototypes ---
namespace insertion {
  void sort (std::vector<int> values);
}

#endif
