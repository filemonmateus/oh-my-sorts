#ifndef HEAP_HPP
#define HEAP_HPP

#include <vector>

// --- interfaces / prototypes ---
namespace heap {
  void swap (int& x, int& y);
  void maxheap (std::vector<int>& values);
  void heapify (std::vector<int>& values, int size, int index);
  void sort (std::vector<int> values);
}

#endif
